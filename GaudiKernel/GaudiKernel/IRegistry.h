#ifndef GAUDIKERNEL_IREGISTRY_H
#define GAUDIKERNEL_IREGISTRY_H

// Include files
#include "GaudiKernel/RegistryEntry.h"

using IRegistry = DataSvcHelpers::RegistryEntry;
#endif // KERNEL_IREGISTRY_H
